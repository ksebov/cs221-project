import tensorflow as tf

from tensorflow.python.ops.rnn_cell import DropoutWrapper
from tensorflow.python.ops import variable_scope as vs
from tensorflow.python.ops import rnn_cell

from modules import masked_softmax


class BiDafAttn(object):
    """Module for BiDaf attention."""

    def __init__(self, hidden_size, keep_prob):
        """
        Inputs:
          keep_prob: tensor containing a single scalar that is the keep probability (for dropout)
          key_vec_size: size of the key vectors. int        
          value_vec_size: size of the value vectors. int
        """
        self.keep_prob = keep_prob
        self.h = hidden_size

    def build_graph(self, q, q_mask, c, c_mask):
        """
        c: Context

        For each key, return an attention distribution and an attention output vector.

        Inputs:
          q: Tensor shape (batch_size, M, value_vec_size).
          q_mask: Tensor shape (batch_size, M).
            1s where there's real input, 0s where there's padding
          c: Tensor shape (batch_size, N, value_vec_size)

        Outputs:
          attn_dist: Tensor shape (batch_size, N, M).
            For each key, the distribution should sum to 1,
            and should be 0 in the value locations that correspond to padding.
          output: Tensor shape (batch_size, N, hidden_size).
            This is the attention output; the weighted sum of the questions
            (using the attention distribution as weights).
        """
        with vs.variable_scope("BiDafAttn"):
            q_shape = q.get_shape().as_list()
            assert(q_shape[0] is None) # batch
            assert(q_shape[2] == self.h)
            M = q_shape[1]

            c_shape = c.get_shape().as_list()
            assert(c_shape[0] is None) # batch
            assert(c_shape[2] == self.h)
            N = c_shape[1]

            # Calculate attention distribution
            S = tf.matmul(c, q, transpose_b=True)

            s_c = tf.stack([c]*M, 2)
            s_q = tf.stack([q]*N, 1)
            s_cq = tf.multiply(s_c, s_q)
            print s_c.get_shape().as_list(), [None, N, M, self.h]
            assert(s_c.get_shape().as_list() == [None, N, M, self.h])
            print s_q.get_shape().as_list(), [None, N, M, self.h]
            assert(s_q.get_shape().as_list() == [None, N, M, self.h])
            assert(s_cq.get_shape().as_list() == [None, N, M, self.h])
            S = tf.concat([s_c, s_q, s_cq], 3)

            h3 = self.h*3
            assert(S.get_shape().as_list() == [None, N, M, h3])

            w_sim = tf.Variable(tf.ones([h3, 1]), name="w_sim")
            S = tf.reshape(S, [-1, h3]) 
            S = tf.matmul(S, w_sim)
            S = tf.reshape(S, [-1, N, M ])

            print S.get_shape().as_list(), [None, N, M]
            assert(S.get_shape().as_list() == [None, N, M])

            q_mask = tf.expand_dims(q_mask, 1)
            masked_S, alpha = masked_softmax(S, q_mask, 2) # take softmax over M

            # Use attention distribution to take weighted sum of questions
            a = tf.matmul(alpha, q)
            assert(a.get_shape().as_list() == [None, N, self.h])

            m = tf.reduce_max(masked_S, 2) # max over M
            assert(m.get_shape().as_list() == [None, N])

            _, beta = masked_softmax(m, c_mask, 1)
            assert(beta.get_shape().as_list() == [None, N])

            c_prime = tf.matmul(c, tf.expand_dims(beta, 2), transpose_a=True)
            assert(c_prime.get_shape().as_list()==[None, self.h, 1])
            c_prime = tf.reshape(c_prime, [-1, 1, self.h])

            blended_reps = tf.concat([c, a, tf.multiply(c, a), tf.multiply(c, c_prime)], axis=2) # (batch_size, context_len, hidden_size*4)
            assert(blended_reps.get_shape().as_list()==[None, N, self.h*4])

            # Apply dropout
            blended_reps = tf.nn.dropout(blended_reps, self.keep_prob)

            return blended_reps

class BiDafModelling(object):
    def __init__(self, hidden_size, keep_prob, rnn='lstm', n_layers = 2):
        """
        Inputs:
          hidden_size: int. Hidden size of the RNN
          keep_prob: Tensor containing a single scalar that is the keep probability (for dropout)
        """
        self.hidden_size = hidden_size
        self.keep_prob = keep_prob
        self.n_layers = n_layers
        self.rnn = rnn

    def create_rnn_cell(self):
        if self.rnn == 'lstm':
            cell = rnn_cell.LSTMCell(self.hidden_size)
        elif self.rnn == 'gru':
            cell = rnn_cell.GRUCell(self.hidden_size)
        else:
            assert(False) # Unknown RNN type

        return DropoutWrapper(cell, input_keep_prob=self.keep_prob)

    def build_graph(self, inputs, masks):
        with vs.variable_scope("ModellingLayer"):
            lens = tf.reduce_sum(masks, reduction_indices=1)
            assert(lens.get_shape().as_list()    == [None])

            x = inputs
            for l in range(self.n_layers):
                cell_fw = self.create_rnn_cell()
                cell_bw = self.create_rnn_cell()

                (fw_out, bw_out), _ = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, x, lens, dtype=tf.float32, scope="layer_"+str(l))

                x = tf.concat([fw_out, bw_out], 2)

            out = tf.nn.dropout(x, self.keep_prob)

            return out


class CoAttn(object):
    """Module for Coattention attention."""

    def __init__(self, hidden_size, keep_prob):
        self.keep_prob = keep_prob
        self.h = hidden_size

    def build_graph(self, q, q_mask, c, c_mask, use_sentinel):
        with vs.variable_scope("CoAttn"):
            q_shape = q.get_shape().as_list()
            assert(q_shape[0] is None) # batch
            assert(q_shape[2] == self.h)
            M = M1 = q_shape[1]

            c_shape = c.get_shape().as_list()
            assert(c_shape[0] is None) # batch
            assert(c_shape[2] == self.h)
            N = N1 = c_shape[1]

            q_prime = tf.contrib.layers.fully_connected(q, num_outputs=self.h, activation_fn=tf.nn.tanh)

            if False: #todo sentinel
                q_sentinel = tf.get_variable("q_sentinel", shape=[1, 1, self.h], initializer=tf.contrib.layers.xavier_initializer())
                q_prime = tf.stack([q_prime, q_sentinel], 2)
                q_mask = tf.stack(q_mask, [1])
                N1 += 1

                c_sentinel = tf.get_variable("c_sentinel", shape=[1, 1, self.h], initializer=tf.contrib.layers.xavier_initializer())
                c = tf.stack([c, c_sentinel], 2)
                c_mask = tf.stack(c_mask, [1])
                M1 += 1

            assert(q_prime.get_shape().as_list() == [None, M1, self.h])
            assert(c.get_shape().as_list() == [None, N1, self.h])

            L = tf.matmul(c, q_prime, transpose_b=True)
            assert(L.get_shape().as_list() == [None, N1, M1])

            q_mask = tf.expand_dims(q_mask, 1)
            _, alpha = masked_softmax(L, q_mask, 2) # take softmax over M
            assert(alpha.get_shape().as_list() == [None, N1, M1])

            a = tf.matmul(alpha, q_prime)
            assert(a.get_shape().as_list() == [None, N1, self.h])

            c_mask = tf.expand_dims(c_mask, 2)
            _, beta = masked_softmax(L, c_mask, 1) # take softmax over M
            assert(beta.get_shape().as_list() == [None, N1, M1])

            b = tf.matmul(beta, c, transpose_a=True)
            print b.get_shape().as_list(), [None, M1, self.h]
            assert(b.get_shape().as_list() == [None, M1, self.h])

            s = tf.matmul(alpha, b)
            assert(s.get_shape().as_list() == [None, N1, self.h])

            blended_reps = tf.concat([s, a], axis=2) # (batch_size, context_len, hidden_size*2)
            assert(blended_reps.get_shape().as_list()==[None, N1, self.h*2])

            # Apply dropout
            blended_reps = tf.nn.dropout(blended_reps, self.keep_prob)

            return blended_reps

